<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->
  <meta name="description" content="$Description">
  <meta name="author" content="Human Connections Group | Recruitment Agency">

  <title>$Title | Recruitment Agency</title>
  <link rel="apple-touch-icon" sizes="180x180" href="{$BaseHref}apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="{$BaseHref}favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="{$BaseHref}favicon-16x16.png">
  <meta name=”robots” content="index, follow">
  <meta property=”og:type” content="website"/>
  <meta property=”og:title” content="$Title  | Recruitment Agency" />
  <meta property=”og:description” content="$Description"/>
  <meta property=”og:image” content="{$BaseHref}images/Logo.png"/>
  <meta property=”og:url” content="https://www.humanconnectionsgroup.com/"/>
  <meta property=”og:site_name” content="Human Connections Group, Duendin, New Zealand  | Recruitment Agency" />
  <link rel=”canonical” href=”$URL” />

  <!-- Bootstrap CSS -->
  <!-- https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css -->
  <link rel="stylesheet" href="{$BaseHref}_resources/themes/hcg/vendor/bootstrap/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="{$BaseHref}_resources/themes/hcg/css/styles.css">
  <link rel="stylesheet" href="{$BaseHref}_resources/themes/hcg/css/normalize.css">

  <meta name="theme-color" content="#fafafa">
</head>

<body class="$ClassName">
  
  <!--[if IE]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->
  <div class="container">
    <!-- New Jobs Pop Up Form -->
    <% include PopUpForm %>
  </div>
  <!-- Navigation on laptop and desktop screens-->

  <!-- Navigation on ipds screens and smaller -->
  <% include SmallNavigation %>

  $Layout

  <footer class="mt-5">
    <div class="container-fluid pt-5">
      <div class="row">

        <div class="col-xs-12 col-md-12 offset-lg-2">
          
          <div class="col-md-3 col-sm-6 col-lg-2 col-6 p-0 float-left mb-3">
            <h5 class="mb-4">Business Info</h5>
            <ul class="list-group">
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">1 Bath Street</li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">Dunedin 9016</li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">Monday - Friday</li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">9.00am - 5.00pm</li>
            </ul>
          </div>

          <div class="col-md-3 col-sm-6 col-lg-2 col-6 p-0 mb-3 float-left">
            <h5 class="mb-4">Phone</h5>
            <ul class="list-group">
            
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">Emily Richards</li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">+64 27 959 6847</li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">Victoria Robertson</li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">+64 29 200 1595</li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2"><a href="mailto:jobs@humanconnectionsgroup.com">Email Us</a></li>
            </ul>
          </div>

          <div class="col-md-3 col-sm-6 col-lg-2 col-6 mb-3 p-0 float-left">
            <h5 class="mb-4">Useful Links</h5>
            <ul class="list-group">
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="{$BaseHref}">Home</a>
              </li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="{$BaseHref}">About</a>
              </li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="{$BaseHref}">Hiring</a>
              </li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="{$BaseHref}">Candidates</a>
              </li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="{$BaseHref}">HR</a>
              </li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="{$BaseHref}">Training</a>
              </li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="{$BaseHref}">Pricing</a>
              </li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="{$BaseHref}">Contact</a>
              </li>
            </ul>
          </div>

          <div class="col-md-3 col-sm-6 col-lg-2 col-6 mb-3 p-0 float-left">
            <h5 class="mb-4">Check Out Stepping Stone</h5>
            <ul class="list-group">
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                Our Job Seeker & Career Support partner
              </li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a class="navbar-brand" href="#"><img class="img-fluid" src="{$BaseHref}_resources/themes/hcg/img/steppingStoneLogo.png" alt="Human Connections Logo - Recruitment Agency Dunedin New Zealand" /></a>
              </li>
            </ul>
          </div>
        </div>

        <div class="col-md-12">
          
        </div>

      </div>
    </div>
    <div class="babyFooter py-4 d-flex justify-content-center align-items-center">
      <!-- <a class="mr-4" href="../privacy.html">Privacy &amp; terms</a>
      <a href="../sitemap.xml">Sitemap</a> -->
      <p>Human Connections Group © 2019 | Website Design by <a href="https://www.punchmarketing.co.nz/">Punch Marketing</a> | All Rights Reserved. | <a href="{$BaseHref}privacy">Privacy Statement</a> | <a href="{$BaseHref}terms-of-trade">Terms of Trade</a></p>
    </div>
  </footer>

  <script src="{$BaseHref}_resources/themes/hcg/js/modernizr-3.7.1.min.js"></script>
  <!-- <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="{$BaseHref}_resources/themes/hcg/js/jquery-3.4.1.min.js"><\/script>')</script> -->
  
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="{$BaseHref}_resources/themes/hcg/js/plugins.js"></script>
  <script src="{$BaseHref}_resources/themes/hcg/js/main.js"></script>
  <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
  <!-- <script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto'); ga('set','transport','beacon'); ga('send', 'pageview')
  </script>
  <script src="https://www.google-analytics.com/analytics.js" async></script> -->
</body>

</html>

