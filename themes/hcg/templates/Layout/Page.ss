<% include PageTitle %>
<main role="main" class="container my-5">
	<div class="row">
	    
	    <div class="col-12 mt-5">
	        $Form
	        $Content
	    </div>
	    
	    <% if $ClassName == 'SilverStripe\UserForms\Model\UserDefinedForm' %>
	    	<div class="col-12 mt-5">
	    		<h2>Latest Jobs</h2>
	    	</div>
	    	<div class="col">
			    <div class="feature-box fbox-center fbox-bg fbox-plain">
			        <div class="testi-content">
						<p>Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum!</p>
						<div class="testi-meta">
						John Doe
						<span>XYZ Inc.</span>
						</div>
					</div>
			    </div>
		    </div>
		    <div class="col">
			    <div class="feature-box fbox-center fbox-bg fbox-plain">
			        <div class="testi-content">
						<p>Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum!</p>
						<div class="testi-meta">
						John Doe
						<span>XYZ Inc.</span>
						</div>
					</div>
			    </div>
		    </div>
		    <div class="col">
			    <div class="feature-box fbox-center fbox-bg fbox-plain">
			        <div class="testi-content">
						<p>Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum!</p>
						<div class="testi-meta">
						John Doe
						<span>XYZ Inc.</span>
						</div>
					</div>
			    </div>
		    </div>
	    <% end_if %>

	</div>
</main>