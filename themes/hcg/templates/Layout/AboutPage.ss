<div class="container-fluid px-0">
    <header>
        <h1 class="d-flex align-items-center justify-content-center">About Us</h1>
        <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
            <source src="{$BaseHref}_resources/themes/hcg/videos/HCG.mp4" type="video/mp4">
        </video>
    </header>
</div>

<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6 mt-5 pr-lg-5">
                <hr class="new4">
                <h2>Recruitment & HR Specialists</h2>
                $ContentBlockOne
            </div>
            <div class="col-sm-12 col-md-6 mt-5">
                <div>
                    <img class="img-fluid" src="$Image.Fill(600,400).URL"/>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid twoColumnBanner mt-5">
 	<div class="container">
    	<div class="row">
			<div class="col-sm-12 col-md-6 twoColumnBanner-Col-1">
				<hr class="new6">
                <h2 class="mt-3">A different approach</h2>
                $ContentBlockTwo
        	</div>
        	<div class="col-sm-12 col-md-6 twoColumnBanner-Col-2">
                    $ContentBlockThree
        	</div>
		</div>
    </div>
</div>

<div class="container-fluid mt-5 my-5">
    <div class="MeetTheTeam container">
        <div class="row">
            <div class="col-sm-12 text-center mb-5">
                <h2 class="mt-2">Meet the Team</h2>
            </div>
        </div>
        <div class="row">
            <% loop $StaffMembers %>
                
                <div class="col-sm-12 col-md-4 mb-3">
                    
                    <% if $Up.IsMobile = 0 %>
                        
                        <style>
                            .effect_{$ID}:hover {
                                background-image: url('$CandidPic.Fill(250,300).URL') !important;
                            }
                        </style>

                    <% end_if %>

                    <div class="text-center">
                        <div class="pic-container">
                            <div class="picture effect_{$ID}" style="background-image: url('$Pic.Fill(250,300).URL');"></div>
                        </div>
                        <h3 class="mt-5">$Name</h3>
                        <hr class="new5 mt-4 mb-4">
                        $Description<br>
                        <a href="tel:$Phone" class="tel mt-2">$Phone</a> // 
                        <a href="mailto:$Email">$Email</a>
                    </div>
                </div>

            <% end_loop %>
        </div>
    </div>
</div>