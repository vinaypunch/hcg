<% include PageTitle %>
<main role="main" class="container my-5">
	<div class="row">

	    <div class="col-12 mt-5 clearfix">
		    <ul id="portfolio-filter" class="portfolio-filter clearfix" data-container="#portfolio">
		        <li class="">
		        	<a href="#" data-filter="*">Show All</a>
		        </li>
		        <% loop $GetTags %>
                    <li class="">
                    	<a href="$Link" data-filter=".pf-{$CodeName}">
                    		$PublicName
                    	</a>
                	</li>
                <% end_loop %>
		    </ul>
		    <div class="clear"></div>

		    <div id="portfolio" class="portfolio grid-container portfolio-nomargin clearfix">
		        
		        <% loop $Children %>
			        <article class="portfolio-item <% loop $Tags %>pf-{$CodeName}<% end_loop %>">
			            <div class="portfolio-image">
			                <a href="$Link">
			                	<% if $JobImage %>
			                		<img src="$JobImage.ScaleWidth(300).URL" alt="Open Imagination">
		                		<% end_if %>
			                </a>
			            </div>
			            <div class="portfolio-desc">
			                <h3><a href="portfolio-single.html">$Title</a></h3>
			                <% loop $Tags %>
			                	<span>$PublicName<% if $Last %><% else %>, <% end_if %></span>
			                <% end_loop %>
			            </div>
			        </article>
		        <% end_loop %>
		    </div>
		</div>

	</div>
</main>