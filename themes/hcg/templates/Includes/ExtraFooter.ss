<div class="container-fluid noPadding threeColumnBanner">
    <div class="mediumFix container">
        <div class="row d-flex justify-content-sm-center justify-content-md-center justify-content-lg-around">
            <div class="col-sm-12 col-md-8 col-lg-4">
                <div class="text-center border-right-hcg pr-lg-5">
                    <h3 class="mt-5">Recruitment<br> made easy</h3>
                    <hr class="new5 my-3">
                    <p>Human Connections Group is Dunedin’s most cost effective recruitment agency and human resources</p>
                    <button type="button" class="jobBoard btn btn-primary btn-sm">Connect</button>
                </div>
            </div>
            <div class="col-sm-12 col-md-8 col-lg-4">
                <div class="text-center border-right-hcg pr-lg-5">
                    <h3 class="mt-5">Human<br> Resources</h3>
                    <hr class="new5 my-3">
                    <p>Human Connections Group is Dunedin’s most cost effective recruitment agency and human resources</p>
                    <button type="button" class="jobBoard btn btn-primary btn-sm">Connect</button>
                </div>
            </div>
            <div class="col-sm-12 col-md-8 col-lg-4">
                <div class="text-center pr-lg-5">
                    <h3 class="mt-5">Training & Development</h3>
                    <hr class="new5 my-3">
                    <p>Human Connections Group is Dunedin’s most cost effective recruitment agency and human resources</p>
                    <button type="button" class="jobBoard btn btn-primary btn-sm">Connect</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid p-0 customersBanner mt-5">
    <div class="mediumFix container">
        <div class="row">
            <div class="col-sm-12 col-md-12 text-center">
                <h2 class="bold-hcg">A few of our customers</h2>
            </div>
        </div>
        <div class="row">
            <div class="owl-carousel">
                <% loop $CustomerCarouselItems %>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-5">
                                $SucessPlacementImage.ScaleWidth(220)
                            </div>
                            <div class="col-5">
                                $CompanyLogoImage.ScaleWidth(220)
                            </div>
                        </div>
                    </div>
                <% end_loop %>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
      $(".owl-carousel").owlCarousel(
        {
            center: false,
            items:2,
            loop:true,
            margin:10,
            autoplay:true,
            autoplayTimeout:3500,
            autoplayHoverPause:false,
            responsive:{
                600:{
                    items:2
                }
            }
        }
        );
    });
</script>