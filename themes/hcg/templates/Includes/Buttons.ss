<a href="{$BaseHref}job-board">
	<button type="button" class="bold-hcg jobBoard btn btn-primary btn-sm float-lg-right py-1 px-3">JOB BOARD</button>
</a>
<a href="https://www.facebook.com/HumanConnectionsGroup/">
	<img src="{$BaseHref}_resources/themes/hcg/img/Exports/HCG_Website_Facebook.png" class="float-lg-right py-1 pl-3 p-0" alt="Facebook Logo"/>
</a>
<a href="https://nz.linkedin.com/company/human-connections-group">
	<img src="{$BaseHref}_resources/themes/hcg/img/Exports/HCG_Website_LinkedIn.png" class="float-lg-right py-1 pl-3 pr-0" alt="LinkedIn Logo"/>
</a>