List of things to do for HCG:

1. Descriptions for each page need to be added in when the DB and CMS is set up - or perhaps when you are creating new pages.
3. Make the Copyright Year dynamic in footer.
5. Where does Job Board and Meet the Team link to on HomePage.ss?
6. The staff images on the home page shold have the green thing in the corner.
8. Images of customers needed. How will they add to this in the future.
9. If you need a drop down menu itm see this link - https://bootstrapious.com/p/bootstrap-multilevel-dropdown
16. I think the video in the header is a bad idea - needs cropping if it's going to work. Not sure how to do that easily.
18. Verify the Web masters account and submit a sitemap.
19. Add the white hand to the split screen slider.