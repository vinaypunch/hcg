<?php

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TextField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;

class StaffMember extends DataObject {

    private static $table_name = 'StaffMember';
    
	private static $db = [
        'Name' => 'Varchar',
        'Description' => 'HTMLText',
        'Phone' => 'Varchar',
        'Email' => 'Varchar'
    ];

    private static $has_one = [
        'Pic' => Image::class,
        'CandidPic'=> Image::class,
    ];

    private static $owns = [
        'Pic',
        'CandidPic'
    ];

    public function getCMSFields()
    {
        return FieldList::create(
            TextField::create('Name', 'Name'),
            TextField::create('Phone', 'Phone'),
            TextField::create('Email', 'Email'),
            HTMLEditorField::create('Description', 'Description of Person'),
            UploadField::create('Pic', 'Pic - should be 250px by 300 px wide')->setFolderName('staff_images'),
            UploadField::create('CandidPic', 'CandidPic - should be 250px by 300 px wide')->setFolderName('staff_images')
        );
    }
}
