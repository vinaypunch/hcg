<?php

namespace {

    use SilverStripe\UserForms\Control\UserDefinedFormController;
    
    class ContactPageController extends UserDefinedFormController
    {

        private static $allowed_actions = [
        ];

        protected function init()
        {
            parent::init();
            // You can include any CSS or JS required by your project here.
            // See: https://docs.silverstripe.org/en/developer_guides/templates/requirements/
        }
    }
}
