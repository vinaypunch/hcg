<?php

namespace {

    use SilverStripe\CMS\Controllers\ContentController;
    use SilverStripe\Forms\FieldList;
    use SilverStripe\Forms\Form;
    use SilverStripe\Forms\FormAction;
    use SilverStripe\Forms\RequiredFields;
    use SilverStripe\Forms\TextField;
    use SilverStripe\Forms\EmailField;
    use SilverStripe\Forms\FileField;
    use SilverStripe\Control\Email\Email;
    use SilverStripe\AssetAdmin\Forms\UploadField;
    use  HudhaifaS\Forms\FrontendFileField;
    use SilverStripe\Assets\File;

    class PageController extends ContentController
    {

        private static $allowed_actions = [

           /* 'randomNumber',
            'getQuote',
            'StaffMembers',*/
            'HiringForm'/*,
            'Cut',
            'CustomerCarouselItems'*/
        ];

        protected function init()
        {
            parent::init();
            $detect = new Mobile_Detect;
            if ( $detect->isMobile() ) {
                $this->IsMobile = 1;
            }else{
                $this->IsMobile = 0;
            }
        }

        public function randomNumber()
        {
            $randomNumber = rand();
            return $randomNumber;
        }

        public function getQuote(){

            $oneTestimonial = Testimonial::get()->Limit(1);
            return $oneTestimonial;
        }

        public function StaffMembers(){

            $StaffMembers = StaffMember::get();
            return $StaffMembers;
        }

        public function candidate()
        {
            return $this::curr()->getRequest()->getVar('candidate');
        }

        public function CustomerCarouselItems(){

            $CarouselItems = CustomerCarousel::get();
            return $CarouselItems;
        }

        public function HiringForm() {
            
            $CVUploadField = FrontendFileField::create('CV','Upload Your CV')->setAttribute('class','btn-sm mt-3')->setFolderName('hiringformcvs');

             $form = Form::create(
                $this,
                __FUNCTION__,
                FieldList::create(
                    EmailField::create('Email','Email'),
                    $CVUploadField
                ),
                FieldList::create(
                    FormAction::create('submitHiringForm','Submit')->setAttribute('class','btn-sm mt-3')
                ),
                RequiredFields::create('Email')
            );
            
            $form->setTemplate('PopUpFormTemplate');

            return $form;

        }
        
        public function submitHiringForm($data, $form) {
            
            $cvcontent = file_get_contents($data['CV']['tmp_name']);
            $file = File::create();
            $file->setFromString($cvcontent, $data['CV']['name']);
            $file->write();

            $email = new Email(); 
            $email->setTo('vinay@punchmarketing.co.nz');
            $email->setFrom($data['Email']);
            $email->setSubject("Candidate");
            $email->addAttachmentFromData(
                $file->getString(),   // stream of file contents
                $file->getFilename(), // original filename
                $file->getMimeType()  // mime type
            );

            $messageBody =  "";

            $email->setBody($messageBody);
            $email->send();
            $this->redirect('job-board/?candidate=thankyou');
        }

        public function Cut($string, $count) {

            $cutString = substr($string, 0, $count);
            return $cutString;

        }
    }
}
