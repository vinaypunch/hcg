<?php

namespace {

    use SilverStripe\CMS\Controllers\ContentController;
    use SilverStripe\View\Requirements;
    use SilverStripe\Forms\FieldList;
    use SilverStripe\Forms\TextField;
    use SilverStripe\Forms\UploadField;
    use SilverStripe\Forms\EmailField;
    use SilverStripe\Forms\TextareaField;
    use SilverStripe\Forms\FormAction;
    use SilverStripe\Forms\Form;
    use SilverStripe\Forms\RequiredFields;
    use SilverStripe\Control\Email\Email;
    use HudhaifaS\Forms\FrontendFileField;

    class JobPageController extends PageController
    {
        private static $allowed_actions = ['ApplicationForm'];

        protected function init()
        {
            parent::init();
        }

        public function ApplicationForm(){

        	$CVUploadField = FrontendFileField::create('CV','Upload Your CV')->setFolderName($this->Title . '_cvs');
        	$CoverLetterUploadField = FrontendFileField::create('CoverLetter','Upload Your Cover Letter')->setFolderName($this->Title . '_coverletter');

        	$form = Form::create(
	            $this,
	            __FUNCTION__,
	            FieldList::create(
	                TextField::create('FullName','')->setAttribute('placeholder','Full Name *')->addExtraClass('form-control'),
					EmailField::create('Email','')->setAttribute('placeholder','Email *')->addExtraClass('form-control'),
					TextField::create('Phone','')->setAttribute('placeholder','Phone *')->addExtraClass('form-control'),
					$CVUploadField,
					$CoverLetterUploadField
	            ),

	            FieldList::create(
	                FormAction::create('submitApplicationForm','Apply Now')->addExtraClass('button button-3d button-large btn-block nomargin')
	            ),

	            RequiredFields::create('FullName','Phone','Message')
	        );
			
			$form->setTemplate('ApplicationFormTemplate');

	        return $form;
        }

        public function submitApplicationForm ($data, $form){

        	$email = new Email(); 

			$email->setTo('vinay@punchmarketing.co.nz');
			$email->setFrom('noreply@humanconnectionsgroup.co.nz');
			$email->setSubject("Job Application - Human Connections Group");
			
			$cvcontent = file_get_contents($data['CV']['tmp_name']);
			$coverlettercontent = file_get_contents($data['CoverLetter']['tmp_name']);

			$cvfile = File::create();
			$cvfile->setFromString($cvcontent, $data['CV']['name']);
			$cvfile->write();

			$coverletterfile = File::create();
			$coverletterfile->setFromString($cvcontent, $data['CoverLetter']['name']);
			$coverletterfile->write();

			$messageBody = "<p><strong>Full Name:</strong> {$data['FullName']}</p>  
			<p><strong>Email:</strong> {$data['Email']}</p> 
			<p><strong>Phone:</strong> {$data['Phone']}</p>"; 

			$email->setBody($messageBody);

			$email->addAttachmentFromData(
			    $cvfile->getString(),   // stream of file contents
			    $cvfile->getFilename(), // original filename
			    $cvfile->getMimeType()  // mime type
			);

			$email->addAttachmentFromData(
			    $coverletterfile->getString(),   // stream of file contents
			    $coverletterfile->getFilename(), // original filename
			    $coverletterfile->getMimeType()  // mime type
			);

			$email->send(); 
			
			$form->sessionMessage('Thank you for your enquiry. We will be in touch within 48 hours.', 'success');

	        return $this->redirectBack();
        }
    }
}
