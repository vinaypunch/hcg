<?php

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TextField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;

class CustomerCarousel extends DataObject {

    private static $table_name = 'CustomerCarousel';

    private static $summary_fields = [
        'Name',
    ];
    
    private static $has_one = [
        "SucessPlacementImage" => Image::class,
        "CompanyLogoImage" => Image::class
    ];

	private static $db = [
        'Name' => 'Varchar',
    ];

    private static $owns = [
        "SucessPlacementImage",
        "CompanyLogoImage"
    ];

    public function getCMSFields()
    {
        return FieldList::create(
            TextField::create('Name', 'Company Name'),
            UploadField::create('SucessPlacementImage', 'Images should be no smaller than 220px wide')->setFolderName('CarouselImages'),
            UploadField::create('CompanyLogoImage', 'Images should be no smaller than 220px wide')->setFolderName('CarouselImages')
        );
    }
}
