<?php

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TextField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;

class Table extends DataObject {

    private static $table_name = 'Table';
    private static $has_one = [
        'Page' => SiteTree::class
    ];
	private static $db = [
        'Title' => 'Varchar',
        'Description' => 'HTMLText',
        'TableContent' => 'HTMLText'
    ];

    private static $owns = [
    ];

    public function getCMSFields()
    {
        return FieldList::create(
            TextField::create('Title', 'Title to be shown for table'),
            HTMLEditorField::create('Description', 'Description of Table if required'),
            HTMLEditorField::create('TableContent', 'Table content itself')
        );
    }
}
