<?php

namespace {

	use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Forms\GridField\GridField;
	use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;

    class PricingPage extends Page
    {
        private static $db = [];

        private static $has_many = [

        	'Tables' => Table::class
        ];

        private static $owns = [
            'Tables'
        ];

        public function getCMSFields()
		{
			
			$fields = parent::getCMSFields();
			
			$fields->addFieldToTab('Root.Tables', GridField::create(
	            'Tables',
	            'Tables',
	            $this->Tables(),
	            GridFieldConfig_RecordEditor::create()
	        ));

			return $fields;
		}
    }
}
