<?php

namespace {

    use SilverStripe\CMS\Model\SiteTree;

    class Page extends SiteTree
    {
        private static $db = [
        	'IsMobile' => 'Boolean',
        	'IsTablet' => 'Boolean'
        ];

        private static $has_one = [];
    }
}
