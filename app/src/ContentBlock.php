<?php

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TextField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;

class ContentBlock extends DataObject {

    private static $table_name = 'ContentBlock';
    private static $has_one = [
        "Page" => SiteTree::class,
    ];
	private static $db = [
        'Title' => 'Varchar',
        'ContentDescription' => 'HTMLText'
    ];

    private static $owns = [
        'Page',
    ];

    public function getCMSFields()
    {
        return FieldList::create(
            TextField::create('Title', 'Title to be shown on page'),
            HTMLEditorField::create('ContentDescription', 'Description of Content')
        );
    }
}
