<?php

namespace {

    use SilverStripe\UserForms\Model\UserDefinedForm;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
    use SilverStripe\Forms\TextField;

    class ContactPage extends UserDefinedForm
    {
        private static $db = [
        	'MapOne' => 'HTMLText',
        	'MapTwo' => 'HTMLText',
        	'MapThree' => 'HTMLText',
        	'LocationOne' => 'Varchar',
        	'LocationTwo' => 'Varchar',
        	'LocationThree' => 'Varchar',
        	'NameOne' => 'Varchar',
        	'NameTwo' => 'Varchar',
        	'NameThree' => 'Varchar',
        	'descriptionOne' => 'HTMLText',
        	'descriptionTwo' => 'HTMLText',
        	'descriptionThree' => 'HTMLText'
        ];

        private static $has_one = [];

        private static $owns = [];

        public function getCMSFields()
		{
			$fields = parent::getCMSFields();

			$fields->addFieldToTab("Root.LocationDetails", new TextField( 'NameOne', 'NameOne' ));
            $fields->addFieldToTab("Root.LocationDetails", new TextField( 'NameTwo', 'NameTwo' ));
            $fields->addFieldToTab("Root.LocationDetails", new TextField( 'NameThree', 'NameThree' ));

            $fields->addFieldToTab("Root.LocationDetails", new TextField( 'LocationOne', 'LocationOne' ));
            $fields->addFieldToTab("Root.LocationDetails", new TextField( 'LocationTwo', 'LocationTwo' ));
            $fields->addFieldToTab("Root.LocationDetails", new TextField( 'LocationThree', 'LocationThree' ));

            $fields->addFieldToTab("Root.LocationDetails", new HTMLEditorField( 'MapOne', 'MapOne' ));
            $fields->addFieldToTab("Root.LocationDetails", new HTMLEditorField( 'MapTwo', 'MapTwo' ));
            $fields->addFieldToTab("Root.LocationDetails", new HTMLEditorField( 'MapThree', 'MapThree' ));

            $fields->addFieldToTab("Root.LocationDetails", new HTMLEditorField( 'descriptionOne', 'descriptionOne' ));
            $fields->addFieldToTab("Root.LocationDetails", new HTMLEditorField( 'descriptionTwo', 'descriptionTwo' ));
            $fields->addFieldToTab("Root.LocationDetails", new HTMLEditorField( 'descriptionThree', 'descriptionThree' ));

			return $fields;
		}
    }
}
