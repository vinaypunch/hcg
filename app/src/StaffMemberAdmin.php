<?php

use SilverStripe\Admin\ModelAdmin;

class StaffMemberAdmin extends ModelAdmin 
{

    private static $managed_models = [
        'StaffMember'
    ];

    private static $url_segment = 'staffmembers';

    private static $menu_title = 'StaffMembers Admin';

    public function getList() 
    {
        $list = parent::getList();

        // Always limit by model class, in case you're managing multiple
        if($this->modelClass == 'StaffMember') {
            $list = $list->sort('ID', 'DESC');
        }

        return $list;
    }
}