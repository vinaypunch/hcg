<?php

namespace {

	use SilverStripe\Forms\GridField\GridField;
	use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;

    class TestimonialsPage extends Page
    {
        private static $db = [];

        private static $has_many = [

        	'Testimonials' => Testimonial::class
        ];

        private static $owns = [
            'Testimonials'
        ];

        public function getCMSFields()
		{
			
			$fields = parent::getCMSFields();
			
			$fields->addFieldToTab('Root.Testimonials', GridField::create(
	            'Testimonials',
	            'Testimonials',
	            $this->Testimonials(),
	            GridFieldConfig_RecordEditor::create()
	        ));

			return $fields;
		}
    }
}
