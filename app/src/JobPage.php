<?php

namespace {

	use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Assets\Image;
    use SilverStripe\AssetAdmin\Forms\UploadField;
    use SilverStripe\Forms\GridField\GridField;
    use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
    use SilverStripe\Forms\ListboxField;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
    
    class JobPage extends Page
    {
        private static $db = [];

        private static $has_one = [
            'JobImage' => Image::class
        ];

        private static $has_many = [
            'Tags' => Tag::class,
            'ContentBlocks' => ContentBlock::class
        ];

        private static $owns = [
            'Tags',
            'ContentBlocks',
            'JobImage'
        ];

        public function getCMSFields()
		{
			$fields = parent::getCMSFields();
			
            $source = Tag::get()->map('ID', 'CodeName');

            $fields->addFieldToTab("Root.TagDetails", new ListboxField( $name = "Tags", $title = "Add tags for this Service", $source, $value = 1 ));

            $fields->addFieldToTab("Root.Main", UploadField::create('JobImage' , 'JobImage')->setFolderName($this->Title . '_images'));
            
            $fields->addFieldToTab('Root.ContentBlocks', GridField::create(
                'ContentBlocks',
                'ContentBlocks',
                $this->ContentBlocks(),
                GridFieldConfig_RecordEditor::create()
            ));

			return $fields;
		}
    }
}
