<?php

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\ORM\DataExtension;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;

class CustomSiteConfig extends DataExtension 
{
    
    private static $db = [
        'FooterColumnOne' => 'HTMLText',
        'FooterColumnTwo' => 'HTMLText',
        "FooterColumnThree" => 'HTMLText'
    ];

    public function updateCMSFields(FieldList $fields) 
    {
        $fields->addFieldToTab("Root.Details", new HTMLEditorField("FooterColumnOne", "FooterColumnOne"));
        $fields->addFieldToTab("Root.Details", new HTMLEditorField("FooterColumnTwo", "FooterColumnTwo"));
        // $fields->addFieldToTab("Root.Details", new HTMLEditorField("FooterColumnThree", "FooterColumnThree"));
    }
}