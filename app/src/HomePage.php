<?php

namespace {

	use SilverStripe\AssetAdmin\Forms\UploadField;
    use SilverStripe\Forms\TextField;
    use SilverStripe\Assets\Image;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

    class HomePage extends Page
    {
        private static $db = [

        	'IsMobile' => 'Boolean',
        	'ContentBlockOne' => 'HTMLText',
            'ContentBlockOneTitle' => 'Varchar',
        	'ContentBlockTwo' => 'HTMLText',
            'ContentBlockTwoTitle' => 'Varchar',
        	'ContentBlockThree' => 'HTMLText',
        	'ContentBlockFour' => 'HTMLText'
        ];

        private static $has_one = [

        	'TopImage' => Image::class,
        	'BottomImage' => Image::class
        ];

        private static $owns = [
	        'TopImage',
	        'BottomImage'
	    ];

	    public function getCMSFields()
		{
			$fields = parent::getCMSFields();
	        $fields->addFieldToTab("Root.SliderImages", UploadField::create('TopImage')->setFolderName($this->Title . '_images'));
	        $fields->addFieldToTab("Root.SliderImages", UploadField::create('BottomImage')->setFolderName($this->Title . '_images'));
	        $fields->addFieldToTab("Root.ContentBlocks", new TextField('ContentBlockOneTitle'));
            $fields->addFieldToTab("Root.ContentBlocks", new HTMLEditorField('ContentBlockOne'));
	        $fields->addFieldToTab("Root.ContentBlocks", new HTMLEditorField('ContentBlockTwo'));
            $fields->addFieldToTab("Root.ContentBlocks", new TextField('ContentBlockTwoTitle'));
	        $fields->addFieldToTab("Root.ContentBlocks", new HTMLEditorField('ContentBlockThree'));

			return $fields;
		}
    }
}
