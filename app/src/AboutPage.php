<?php

namespace {

    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
    use SilverStripe\Forms\FieldList;
    use SilverStripe\AssetAdmin\Forms\UploadField;
    use SilverStripe\Assets\Image;
    
    class AboutPage extends Page
    {
        private static $db = [
        	'ContentBlockOne' => 'HTMLText',
        	'ContentBlockTwo' => 'HTMLText',
        	'ContentBlockThree' => 'HTMLText'
        ];

        private static $has_one = [
            'Image'=> Image::class
        ];

        public function getCMSFields()
        {
            $fields = parent::getCMSFields();
            $fields->addFieldToTab("Root.Main", HTMLEditorField::create('ContentBlockOne', 'ContentBlockOne'));
            $fields->addFieldToTab("Root.Main", HTMLEditorField::create('ContentBlockTwo', 'ContentBlockTwo'));
            $fields->addFieldToTab("Root.Main", HTMLEditorField::create('ContentBlockThree', 'ContentBlockThree'));
            $fields->addFieldToTab("Root.Image", UploadField::create('Image', 'Image - should be 600px by 400 px wide')->setFolderName($this->Title . '_images'));
            return $fields;
        }
    }
}
