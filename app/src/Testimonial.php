<?php

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TextField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;

class Testimonial extends DataObject {

    private static $table_name = 'Testimonial';
    private static $has_one = [
        "Logo" => Image::class,
        'Page' => SiteTree::class
    ];
	private static $db = [
        'Text' => 'Varchar',
        'Name' => 'Varchar',
        'Company' => 'Varchar'
    ];

    private static $owns = [
        'Logo'
    ];

    public function getCMSFields()
    {
        return FieldList::create(
            TextField::create('Name', 'Name of Person'),
            TextField::create('Company', 'Company'),
            TextField::create('Text', 'Text or Quote from that person'),
            UploadField::create('Logo', 'Left aligned Logo image')->setFolderName('testimonial_images')
        );
    }
}
