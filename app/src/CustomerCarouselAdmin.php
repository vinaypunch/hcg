<?php

use SilverStripe\Admin\ModelAdmin;

class CustomerCarouselAdmin extends ModelAdmin 
{

    private static $managed_models = [
        'CustomerCarousel'
    ];

    private static $url_segment = 'carousel';

    private static $menu_title = 'Customer Carousel Admin';
}